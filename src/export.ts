import { App } from '@vue/runtime-core'
// @ts-ignore
import JsonViewer from 'vue3-json-viewer'
// @ts-ignore
import VueHighlightJS from 'vue3-highlightjs'
import { VueDraggableNext } from 'vue-draggable-next'

import 'highlight.js/styles/atom-one-dark.css'
import Designer from './components/FormDesigner/Designer.vue'
import DesignerContainer from './components/FormDesigner/DesignerContainer.vue'

export { Designer, DesignerContainer }

const install = (app: App) => {
    // 加载json美化显示
    app.use(JsonViewer)
    // 代码高亮
    app.use(VueHighlightJS)
    app.component(`Designer`, Designer)
    // 拖拽组件
    app.component(`draggable`, VueDraggableNext)
    app.component('DesignerContainer', DesignerContainer)
}

export default {
    install,
}
