import App from './App.vue'
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
// @ts-ignore
import JsonViewer from 'vue3-json-viewer'
import store, { key } from './store/index'
// @ts-ignore
import VueHighlightJS from 'vue3-highlightjs'
import * as ElIcons from '@element-plus/icons'
import { VueDraggableNext } from 'vue-draggable-next'

import 'element-plus/dist/index.css'
import 'highlight.js/styles/atom-one-dark.css'

const app = createApp(App)
// 加载图标
for (const icon in ElIcons) {
    // @ts-ignore
    app.component(icon, ElIcons[icon])
}
app.component(`draggable`, VueDraggableNext)
// 加载elementPlus
app.use(ElementPlus)
// 加载json美化显示
app.use(JsonViewer)
// 加载Vuex
app.use(store, key)
// 代码高亮
app.use(VueHighlightJS)

app.mount('#app')
