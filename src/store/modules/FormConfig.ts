import { Module } from 'vuex'
import { FormConfigTypes, RootStateTypes } from '../../types/ItemMetaTypes'

const inputConfig: Module<FormConfigTypes, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        // 行内表单模式
        inline: false,
        // 	表单域标签的位置， 如果值为 left 或者 right 时，则需要设置 label-width
        labelPosition: 'right',
        // 表单域标签的后缀
        labelSuffix: '',
        // 是否在输入框中显示校验结果反馈图标
        statusIcon: false,
        // 必填字段的标签旁边的红色星号
        hideRequiredAsterisk: false,
        // 是否显示校验错误信息
        showMessage: true,
        // 用于控制该表单内组件的尺寸
        size: 'medium',
        // 禁用
        disabled: false,
    }),
}

export default inputConfig
