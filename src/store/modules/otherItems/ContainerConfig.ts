import { Module } from 'vuex'
import { ContainerType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<ContainerType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 0,
        cols: [
            {
                props: {
                    // 栅栏占据列数
                    span: 12,
                    // 栅格向左移动格数
                    pull: 0,
                    // 栅格向右移动格数
                    push: 0,
                    // 栅格左侧的间隔格数
                    offset: 0,
                },
                formItems: [],
            },
            {
                props: {
                    // 栅栏占据列数
                    span: 12,
                    // 栅格向左移动格数
                    pull: 0,
                    // 栅格向右移动格数
                    push: 0,
                    // 栅格左侧的间隔格数
                    offset: 0,
                },
                formItems: [],
            },
        ],
        tag: 'el-row',
        type: 'Container',
        props: {
            // 垂直排列方式
            align: 'top',
            // 间隔
            gutter: 0,
            // 水平排列方式
            justify: 'start',
        },
        modelValue: 'Container',
        label: '布局容器',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
