import { Module } from 'vuex'
import { ButtonType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<ButtonType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-button',
        type: 'Button',
        props: {
            // 图标
            icon: '',
            type: '',
            size: 'medium',
            // 是否为朴素按钮
            plain: false,
            // 是否为圆角按钮
            round: false,
            // 是否为圆形按钮
            circle: false,
            // 加载中
            loading: false,
            // 是否为圆形按钮
            disabled: false,
        },
        label: '按钮',
        buttonType: 'reset',
        modelValue: 'Button',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
