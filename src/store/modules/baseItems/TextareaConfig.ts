import { Module } from 'vuex'
import { TextareaType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<TextareaType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-input',
        type: 'Textarea',
        props: {
            size: 'medium',
            disabled: false,
            autosize: {
                minRows: 2,
                maxRows: 20,
            },
            readonly: false,
            clearable: false,
            placeholder: '文本域',
        },
        modelValue: 'Textarea',
        label: '文本域',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
