import { Module } from 'vuex'
import { SwitchType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<SwitchType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-switch',
        type: 'Switch',
        props: {
            disabled: false,
            // 宽度
            width: 40,
            // 是否显示加载中
            loading: false,
            // 是否在点内显示图标或文字
            inlinePrompt: false,
            // switch 状态为 on 时所显示图标，设置此项会忽略 active-text
            activeIcon: '',
            // switch 状态为 off 时所显示图标，设置此项会忽略 inactive-text
            inactiveIcon: '',
            // 	switch 状态为 on 时的文字描述
            activeText: '',
            // 	switch 的状态为 off 时的文字描述
            inactiveText: '',
            // 	switch 状态为 on 时的值
            activeValue: true,
            // 	switch的状态为 off 时的值
            inactiveValue: false,
            // 	switch的值为 on 时的颜色
            activeColor: '#409EFF',
            // 	switch的值为 off 的颜色
            inactiveColor: '#C0CCDA',
            // 	switch 边框颜色
            borderColor: '',
        },
        modelValue: 'Switch',
        label: '开关',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
