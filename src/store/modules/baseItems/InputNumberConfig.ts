import { Module } from 'vuex'
import { InputNumberType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<InputNumberType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-input-number',
        type: 'InputNumber',
        props: {
            size: 'medium',
            disabled: false,
            // 最小
            min: 0,
            // 最大
            max: 500,
            // 步长
            step: 1,
            // 是否使用控制按钮
            controls: true,
            // 数值精度
            precision: 0,
        },
        modelValue: 'InputNumber',
        label: '步进器',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
