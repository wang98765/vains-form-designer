import { Module } from 'vuex'
import { CheckboxType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<CheckboxType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-checkbox-group',
        type: 'Checkbox',
        props: {
            disabled: false,
            min: 0,
            max: 5,
            border: false,
        },
        label: '复选框',
        modelValue: 'Checkbox',
        options: [
            {
                value: 1,
                label: '选项一',
            },
            {
                value: 2,
                label: '选项二',
            },
        ],
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
