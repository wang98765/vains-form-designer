import { Module } from 'vuex'
import { RadioType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<RadioType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-radio-group',
        type: 'Radio',
        props: {
            disabled: false,
            border: false,
        },
        modelValue: 'Radio',
        label: '单选框',
        options: [
            {
                value: 1,
                label: '选项一',
            },
            {
                value: 2,
                label: '选项二',
            },
        ],
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
