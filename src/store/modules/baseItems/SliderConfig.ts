import { Module } from 'vuex'
import { SliderType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<SliderType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-slider',
        type: 'Slider',
        props: {
            disabled: false,
            min: 0,
            max: 100,
            step: 1,
            showInput: false,
            showInputControls: true,
            showTooltip: true,
        },
        modelValue: 'Slider',
        label: '滑块',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
