import { Module } from 'vuex'
import { RateType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<RateType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-rate',
        props: {
            max: 5,
            // 只读
            disabled: false,
            // 是否允许半选
            allowHalf: true,
            // 该值及以下是低分
            lowThreshold: 2,
            // 该值及以上是中高分
            highThreshold: 4,
            // 未选中 icon 的颜色
            voidColor: '#C6D1DE',
            // 只读时未选中 icon 的颜色
            disabledVoidColor: '#EFF2F7',
            // icon 的颜色。 若传入数组，共有 3 个元素，为 3 个分段所对应的颜色；若传入对象，可自定义分段，键名为分段的界限值，键值为对应的颜色
            colors: ['#99A9BF', '#F7BA2A', '#FF9900'],
            // 图标组件 若传入数组，共有 3 个元素，为 3 个分段所对应的类名；若传入对象，可自定义分段，键名为分段的界限值，键值为对应的类名
            icons: ['StarFilled', 'StarFilled', 'StarFilled'],
            // 未被选中的图标组件
            voidIcon: 'Star',
            // 禁用状态的未选择图标
            disabledVoidIcon: 'StarFilled',
            // 是否显示辅助文字，若为真，则会从 texts 数组中选取当前分数对应的文字内容
            showText: false,
            // 是否显示当前分数， 是否显示当前分数， show-score 和 show-text 不能同时为真
            showScore: false,
            // 辅助文字的颜色
            textColor: '#1F2D3D',
            // 辅助文字数组
            texts: ['Extremely bad', 'Disappointed', 'Fair', 'Satisfied', 'Surprise'],
        },
        type: 'Rate',
        label: '评分',
        modelValue: 'Rate',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
