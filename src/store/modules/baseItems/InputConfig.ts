import { Module } from 'vuex'
import { InputType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<InputType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-input',
        type: 'Input',
        props: {
            size: 'medium',
            disabled: false,
            readonly: false,
            clearable: false,
            suffixIcon: '',
            prefixIcon: '',
            placeholder: '输入框',
        },
        label: '输入框',
        modelValue: 'Input',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
