import { Module } from 'vuex'
import { PasswordType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<PasswordType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-input',
        type: 'Password',
        props: {
            size: 'medium',
            disabled: false,
            readonly: false,
            clearable: false,
            suffixIcon: '',
            prefixIcon: '',
            showPassword: true,
            placeholder: '密码框',
        },
        modelValue: 'Password',
        label: '密码框',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
