import { Module } from 'vuex'
import { FormDataEnum } from './FormData'
import {
    RootStateTypes,
    FormItemsType,
    CheckboxType,
    PasswordType,
    InputType,
    TextareaType,
    SwitchType,
    RadioType,
    SelectType,
    TimePickerType,
    InputNumberType,
    ColorType,
    VirtualizedSelectType,
    SliderType,
    RateType,
    TimeSelectType,
} from '../../types/ItemMetaTypes'
import { nextTick } from 'vue'

export enum FormItemsEnum {
    NAME_SPACED = 'FormItems',
    ADD_FORM_ITEM = 'addFormItem',
    COPY_FORM_ITEM = 'copyFormItem',
    RESET_FORM_ITEMS = 'resetFormItem',
    REMOVE_FORM_ITEM = 'removeFormItem',
    SET_CURRENT_ITEM = 'setCurrentItem',
    RELOAD_CHANGE_ITEM = 'reloadChangeItem',
}

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace FormItemsEnum {
    export function ModuleFunctionName(config: FormItemsEnum): string {
        return FormItemsEnum.NAME_SPACED + '/' + config
    }
}

const inputConfig: Module<FormItemsType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        items: [],
        changeItem: true,
        currentItem: {},
    }),
    mutations: {
        /**
         * 重置表单项
         * @param state 状态
         * @param items 新的表单项
         */
        [FormItemsEnum.RESET_FORM_ITEMS](
            state: FormItemsType,
            items: Array<
                | CheckboxType
                | InputType
                | PasswordType
                | TextareaType
                | InputNumberType
                | RadioType
                | RateType
                | SwitchType
                | SliderType
                | ColorType
                | SelectType
                | VirtualizedSelectType
                | TimePickerType
                | TimeSelectType
            >
        ) {
            state.items = items
        },
        /**
         * 重置表单项
         * @param state 状态
         */
        [FormItemsEnum.RELOAD_CHANGE_ITEM](state: FormItemsType) {
            state.changeItem = false
            nextTick().then(() => (state.changeItem = true))
        },
        /**
         * 重置表单项
         * @param state 状态
         * @param item 当前选中的表单项
         */
        [FormItemsEnum.SET_CURRENT_ITEM](state: FormItemsType, item: any) {
            state.currentItem = item
        },
    },
    actions: {
        /**
         * 重置表单项
         * @param commit 状态
         */
        [FormItemsEnum.RELOAD_CHANGE_ITEM]({ commit }) {
            commit(FormItemsEnum.RELOAD_CHANGE_ITEM)
        },
        /**
         * 添加表单项
         * @param state 状态
         * @param dispatch 调用actions
         * @param newIndex 新添加的坐标
         */
        [FormItemsEnum.ADD_FORM_ITEM]({ state, dispatch }, newIndex: number) {
            const keyPrev = state.items[newIndex].modelValue
            const key = state.items[newIndex].modelValue + '_' + new Date().getTime()
            state.items[newIndex] = JSON.parse(JSON.stringify(state.items[newIndex]))
            state.items[newIndex].modelValue = key
            dispatch(FormDataEnum.ModuleFunctionName(FormDataEnum.REMOVE_FORM_DATA), keyPrev, { root: true })
        },
        /**
         * 重置表单项
         * @param state 状态
         * @param items 新的表单项
         */
        [FormItemsEnum.RESET_FORM_ITEMS](
            { commit },
            items: Array<
                | CheckboxType
                | InputType
                | PasswordType
                | TextareaType
                | InputNumberType
                | RadioType
                | RateType
                | SwitchType
                | SliderType
                | ColorType
                | SelectType
                | VirtualizedSelectType
                | TimePickerType
                | TimeSelectType
            >
        ) {
            commit(FormItemsEnum.RESET_FORM_ITEMS, items)
        },
        /**
         * 设置当前选中的表单项
         * @param state 状态
         * @param item 当前选中的表单项
         */
        [FormItemsEnum.SET_CURRENT_ITEM]({ commit }, item: any) {
            commit(FormItemsEnum.SET_CURRENT_ITEM, item)
        },
        /**
         * 移除表单项
         * @param state 状态
         * @param dispatch 调用actions
         * @param i 要移除项的下标
         */
        [FormItemsEnum.REMOVE_FORM_ITEM]({ state, dispatch }, i: number) {
            const key: string = state.items[i].modelValue
            state.items.splice(i, 1)
            dispatch(FormDataEnum.ModuleFunctionName(FormDataEnum.REMOVE_FORM_DATA), key, { root: true })
        },
        /**
         * 复制表单项
         * @param state 状态
         * @param dispatch 调用actions
         * @param i 要移除项的下标
         */
        [FormItemsEnum.COPY_FORM_ITEM]({ state, dispatch }, i: number) {
            const formItem = JSON.parse(JSON.stringify(state.items[i]))
            state.items.forEach((e) => (e.selectCurrent = false))
            formItem.modelValue = formItem.type + '_' + new Date().getTime()
            state.items.splice(i + 1, 0, formItem)
            if (formItem.type === 'Checkbox') {
                // @ts-ignore
                dispatch(FormDataEnum.ModuleFunctionName(FormDataEnum.INIT_FORM_DATA), formItem.modelValue, [], {
                    root: true,
                })
            }
            if (formItem.type === 'InputNumber') {
                // @ts-ignore
                dispatch(FormDataEnum.ModuleFunctionName(FormDataEnum.INIT_FORM_DATA), formItem.modelValue, 0, {
                    root: true,
                })
            }
        },
    },
}

export default inputConfig
