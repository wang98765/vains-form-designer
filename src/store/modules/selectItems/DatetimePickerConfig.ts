import { Module } from 'vuex'
import { DatetimePickerType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<DatetimePickerType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-date-picker',
        props: {
            type: 'datetime',
            size: 'medium',
            format: 'YYYY-MM-DD HH:mm:ss',
            editable: true,
            readonly: false,
            disabled: false,
            clearable: true,
            clearIcon: 'CircleClose',
            prefixIcon: 'Clock',
            placeholder: '日期时间',
            rangeSeparator: '-',
            endPlaceholder: '结束时间',
            startPlaceholder: '开始时间',
        },
        type: 'DatetimePicker',
        label: '日期时间',
        modelValue: 'DatetimePicker',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
