import { Module } from 'vuex'
import { TimePickerType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<TimePickerType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-time-picker',
        props: {
            size: 'medium',
            format: 'HH:mm:ss',
            isRange: false,
            editable: true,
            readonly: false,
            disabled: false,
            clearable: true,
            clearIcon: 'CircleClose',
            prefixIcon: 'Clock',
            placeholder: '时间选择器',
        },
        type: 'TimePicker',
        label: '时间选择器',
        modelValue: 'TimePicker',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
