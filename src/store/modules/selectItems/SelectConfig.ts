import { Module } from 'vuex'
import { SelectType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<SelectType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-select',
        type: 'Select',
        props: {
            size: 'medium',
            disabled: false,
            multiple: false,
            clearable: false,
            filterable: false,
            noDataText: '无数据',
            noMatchText: '无数据',
            placeholder: '下拉选择器',
            allowCreate: false,
            multipleLimit: 0,
            collapseTags: false,
            fitInputWidth: false,
        },
        modelValue: 'Select',
        label: '下拉选择器',
        options: [
            {
                value: 1,
                label: '选项一',
            },
            {
                value: 2,
                label: '选项二',
            },
        ],
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
