import { Module } from 'vuex'
import { TimePickerRangeType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<TimePickerRangeType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-time-picker',
        props: {
            size: 'medium',
            format: 'HH:mm:ss',
            // 改变该值会报错，所以分开两种
            isRange: true,
            editable: true,
            readonly: false,
            disabled: false,
            clearable: true,
            clearIcon: 'CircleClose',
            prefixIcon: 'Clock',
            arrowControl: false,
            rangeSeparator: '-',
            endPlaceholder: '结束时间',
            startPlaceholder: '开始时间',
        },
        type: 'TimePickerRange',
        label: '时间范围',
        modelValue: 'TimePickerRange',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
