import { Module } from 'vuex'
import { TimeSelectType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<TimeSelectType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-time-select',
        props: {
            end: '18:00',
            step: '00:30',
            size: 'medium',
            start: '09:00',
            minTime: '00:00',
            maxTime: '',
            editable: true,
            disabled: false,
            clearable: true,
            clearIcon: 'CircleClose',
            prefixIcon: 'Clock',
            placeholder: '时间选择器',
        },
        type: 'TimeSelect',
        label: '时间下拉',
        modelValue: 'TimeSelect',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
