import { Module } from 'vuex'
import { VirtualizedSelectType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<VirtualizedSelectType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-select-v2',
        type: 'VirtualizedSelect',
        props: {
            size: 'medium',
            disabled: false,
            multiple: false,
            clearable: false,
            filterable: false,
            noDataText: '无数据',
            noMatchText: '无数据',
            placeholder: '虚拟选择器',
            allowCreate: false,
            multipleLimit: 0,
            collapseTags: false,
            fitInputWidth: false,
            options: [
                {
                    value: 1,
                    label: '选项一',
                },
                {
                    value: 2,
                    label: '选项二',
                },
            ],
        },
        modelValue: 'VirtualizedSelect',
        label: '虚拟选择器',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
