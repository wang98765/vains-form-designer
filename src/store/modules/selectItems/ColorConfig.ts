import { Module } from 'vuex'
import { ColorType, RootStateTypes } from '../../../types/ItemMetaTypes'

const inputConfig: Module<ColorType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        labelWidth: 100,
        tag: 'el-color-picker',
        type: 'Color',
        props: {
            size: 'medium',
            disabled: false,
            showAlpha: false,
            colorFormat: 'hex',
        },
        modelValue: 'Color',
        label: '颜色选择器',
    }),
    mutations: {},
    actions: {},
}

export default inputConfig
