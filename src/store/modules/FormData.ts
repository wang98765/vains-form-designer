import { Module } from 'vuex'
import { FormDataType, RootStateTypes } from '../../types/ItemMetaTypes'

export enum FormDataEnum {
    NAME_SPACED = 'FormData',
    SET_FORM_DATA = 'setFormData',
    INIT_FORM_DATA = 'initFormData',
    RESET_FORM_DATA = 'resetFormData',
    REMOVE_FORM_DATA = 'removeFormData',
}

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace FormDataEnum {
    export function ModuleFunctionName(config: FormDataEnum): string {
        return FormDataEnum.NAME_SPACED + '/' + config
    }
}

const inputConfig: Module<FormDataType, RootStateTypes> = {
    namespaced: true,
    state: () => ({
        data: {},
    }),
    mutations: {
        /**
         * 设置表单data
         * @param state 状态
         * @param key 表单data的key
         * @param value 表单data的值
         */
        // @ts-ignore
        [FormDataEnum.SET_FORM_DATA](state: FormDataType, key: string, value: any) {
            state.data[key] = value
        },
        /**
         * 设置表单data
         * @param state 状态
         * @param data 表单data
         */
        [FormDataEnum.RESET_FORM_DATA](state: FormDataType, data: FormDataType) {
            state.data = data
        },
        /**
         * 根据key删除该key
         * @param state 状态
         * @param key 表单data的key
         */
        [FormDataEnum.REMOVE_FORM_DATA](state: FormDataType, key: string) {
            delete state.data[key]
        },
        /**
         * 初始化表单data，部分比较特殊，要移除原先存在的key
         * @param state 状态
         * @param key 表单data的key
         * @param value 表单data的值
         */
        // @ts-ignore
        [FormDataEnum.INIT_FORM_DATA](state: FormDataType, key: string, value: any) {
            state.data[key] = value
            if (!key) {
                return
            }
            // 移除原先存在的key
            const keys: Array<string> = key.split('_')
            if (!keys || keys.length !== 2) {
                return
            }
            delete state.data[keys[0]]
        },
    },
    actions: {
        /**
         * 设置表单data
         * @param commit 异步提交
         * @param key 表单data的key
         * @param value 表单data的值
         */
        // @ts-ignore
        [FormDataEnum.SET_FORM_DATA]({ commit }, key: string, value: any) {
            commit(FormDataEnum.SET_FORM_DATA, key, value)
        },
        /**
         * 设置表单data
         * @param commit 异步提交
         * @param data 表单data
         */
        [FormDataEnum.RESET_FORM_DATA]({ commit }, data: FormDataType) {
            commit(FormDataEnum.RESET_FORM_DATA, data)
        },
        /**
         * 根据key删除该key
         * @param commit 异步提交
         * @param key 表单data的key
         * @param value 表单data的值
         */
        [FormDataEnum.REMOVE_FORM_DATA]({ commit }, key: string) {
            commit(FormDataEnum.REMOVE_FORM_DATA, key)
        },
        /**
         * 设置表单data
         * @param commit 异步提交
         * @param key 表单data的key
         * @param value 表单data的值
         */
        // @ts-ignore
        [FormDataEnum.INIT_FORM_DATA]({ commit }, key: string, value: any) {
            commit(FormDataEnum.INIT_FORM_DATA, key, value)
        },
    },
}

export default inputConfig
