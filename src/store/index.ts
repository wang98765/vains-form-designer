import { InjectionKey } from 'vue'
import { RootStateTypes } from '../types/ItemMetaTypes'
import { createStore, useStore as baseUseStore, Store } from 'vuex'

// 获取所有模块，转为数组
const baseFiles = import.meta.globEager('./modules/**.ts')
const modulesFiles = import.meta.globEager('./modules/*/*.ts')
const pathList: string[] = []
let path: string
for (path in baseFiles) {
    pathList.push(path)
}
for (path in modulesFiles) {
    pathList.push(path)
}

// 去除后缀与 前缀./
const modules = pathList.reduce((modules: { [x: string]: any }, modulePath: string) => {
    const moduleName = modulePath.replace(/^\.\/modules\/(.*)\.\w+$/, '$1')
    const value = baseFiles[modulePath] || modulesFiles[modulePath]
    modules[moduleName] = value.default
    return modules
}, {})

export default createStore<RootStateTypes>({ modules })

// Vuex 将store 安装到 Vue 应用中使用了 Vue 的 Provide/Inject 特性，这就是 injection key 是很重要的因素的原因
export const key: InjectionKey<Store<RootStateTypes>> = Symbol('vue-store')

// 封装自己的useStore方法，使用时: import { useStore } from './store' 即可，无需每次导出key
export function useStore() {
    return baseUseStore(key)
}
