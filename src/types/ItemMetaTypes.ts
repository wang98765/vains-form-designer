// 文本域尺寸
export interface Option {
    // 选项值
    value: any
    // 选项显示文字
    label: string
}

interface BasePropsType {
    // 列属性
    col?: ColType
    // 尺寸
    size?: string
    // 禁用
    disabled?: boolean
}

interface BaseFormControlType {
    // 标签
    tag: string
    // 空间类型
    type: string
    // 双向绑定的key
    modelValue: string
    // 是否选中当前控件
    selectCurrent?: boolean
    // 标签文字
    label: string
    // 绑定属性
    props: BasePropsType
    // 是否开启验证
    enableRule?: boolean
    // 验证规则
    rules?: any
    // 标签宽度
    labelWidth: number
}

// 文本域尺寸
export interface Autosize {
    // 最小行数
    minRows?: number
    // 最大行数
    maxRows?: number
}

interface InputPropsType extends BasePropsType {
    // 只读？
    readonly: boolean
    // 可清理?
    clearable: boolean
    // 后缀图标
    suffixIcon: string
    // 前缀图标
    prefixIcon: string
    // 描述输入字段的提示信息
    placeholder: string
}

// 输入框类型
export interface InputType extends BaseFormControlType {
    props: InputPropsType
}

interface TextareaPropsType extends BasePropsType {
    // 自动调整尺寸
    autosize: Autosize
    // 只读？
    readonly: boolean
    // 可清理?
    clearable: boolean
    // 描述输入字段的提示信息
    placeholder: string
    // 控件配置使用
    showAutoSize?: boolean
}

// 文本域
export interface TextareaType extends BaseFormControlType {
    props: TextareaPropsType
}

interface PasswordPropsType extends BasePropsType {
    // 只读？
    readonly: boolean
    // 可清理?
    clearable: boolean
    // 后缀图标
    suffixIcon: string
    // 前缀图标
    prefixIcon: string
    // 描述输入字段的提示信息
    placeholder: string
    // 显示查看密码图标
    showPassword: boolean
}

// 密码框
export interface PasswordType extends BaseFormControlType {
    props: PasswordPropsType
}

interface CheckboxPropsType extends BasePropsType {
    // 最小选择数量
    min: number
    // 最大选择数量
    max: number
    // 显示边框
    border: boolean
}

// 复选框
export interface CheckboxType extends BaseFormControlType {
    props: CheckboxPropsType
    // 选项数组
    options: Array<Option>
}

interface RadioPropsType extends BasePropsType {
    // 显示边框
    border: boolean
}

// 单选框
export interface RadioType extends BaseFormControlType {
    props: RadioPropsType
    // 选项数组
    options: Array<Option>
}

interface InputNumberPropsType extends BasePropsType {
    // 最小
    min: number
    // 最大
    max: number
    // 步长
    step: number
    // 是否使用控制按钮
    controls: boolean
    // 数值精度
    precision: number
}

// 步进器
export interface InputNumberType extends BaseFormControlType {
    props: InputNumberPropsType
}

interface RatePropsType extends BasePropsType {
    // 最大分值
    max: number
    // 是否允许半选
    allowHalf: boolean
    // 该值及以下是低分
    lowThreshold: number
    // 该值及以上是中高分
    highThreshold: number
    // icon 的颜色。 若传入数组，共有 3 个元素，为 3 个分段所对应的颜色；若传入对象，可自定义分段，键名为分段的界限值，键值为对应的颜色
    colors: Array<string> | object
    // 未选中 icon 的颜色
    voidColor: string
    // 只读时未选中 icon 的颜色
    disabledVoidColor: string
    // 图标组件 若传入数组，共有 3 个元素，为 3 个分段所对应的类名；若传入对象，可自定义分段，键名为分段的界限值，键值为对应的类名
    icons: Array<string> | object
    // 未被选中的图标组件
    voidIcon: string
    // 禁用状态的未选择图标
    disabledVoidIcon: string
    // 是否显示辅助文字，若为真，则会从 texts 数组中选取当前分数对应的文字内容
    showText: boolean
    // 是否显示当前分数， 是否显示当前分数， show-score 和 show-text 不能同时为真
    showScore: boolean
    // 辅助文字的颜色
    textColor: string
    // 辅助文字数组
    texts: Array<string>
}

// 评分
export interface RateType extends BaseFormControlType {
    props: RatePropsType
}

interface SwitchPropsType extends BasePropsType {
    // 宽度
    width: number
    // 是否显示加载中
    loading: boolean
    // 是否在点内显示图标或文字
    inlinePrompt: boolean
    // switch 状态为 on 时所显示图标，设置此项会忽略 active-text
    activeIcon: string
    // switch 状态为 off 时所显示图标，设置此项会忽略 inactive-text
    inactiveIcon: string
    // 	switch 状态为 on 时的文字描述
    activeText: string
    // 	switch 的状态为 off 时的文字描述
    inactiveText: string
    // 	switch 状态为 on 时的值
    activeValue: boolean | string | number
    // 	switch的状态为 off 时的值
    inactiveValue: boolean | string | number
    // 	switch的值为 on 时的颜色
    activeColor: string
    // 	switch的值为 off 的颜色
    inactiveColor: string
    // 	switch 边框颜色
    borderColor: string
}

// 开关
export interface SwitchType extends BaseFormControlType {
    props: SwitchPropsType
}

interface SliderPropsType extends BasePropsType {
    // 最小
    min: number
    // 最大
    max: number
    // 步长
    step: number
    // 是否显示输入框，仅在非范围选择时有效
    showInput: boolean
    // 是否显示 tooltip
    showTooltip: boolean
    // 在显示输入框的情况下，是否显示输入框的控制按钮
    showInputControls: boolean
}

// 滑块
export interface SliderType extends BaseFormControlType {
    props: SliderPropsType
}

interface ColorPropsType extends BasePropsType {
    // 是否支持透明度选择
    showAlpha: boolean
    // 写入 v-model 的颜色的格式 hsl / hsv / hex / rgb
    colorFormat: string
}

// 颜色选择器
export interface ColorType extends BaseFormControlType {
    props: ColorPropsType
}

interface SelectPropsType extends BasePropsType {
    // 是否多选
    multiple: boolean
    // 是否可以清空选项
    clearable: boolean
    // 选项为空时显示的文字，也可以使用 empty 插槽设置
    noDataText: string
    // 是否可以筛选
    filterable: boolean
    // 搜索条件无匹配时显示的文字，也可以使用 empty 插槽设置
    noMatchText: string
    // 描述文字
    placeholder: string
    // 是否允许用户创建新条目， 当 filterable 必须为 true 时才会生效。
    allowCreate: boolean
    // 多选时用户最多可以选择的项目数， 为 0 则不限制
    multipleLimit: number
    // 多选时是否将选中值按文字的形式展示
    collapseTags: boolean
    // 下拉框是否与输入框同宽
    fitInputWidth: boolean
}

// 下拉选择器
export interface SelectType extends BaseFormControlType {
    props: SelectPropsType
    // 选项数组
    options: Array<Option>
}

interface VirtualizedSelectPropsType extends BasePropsType {
    // 是否多选
    multiple: boolean
    // 是否可以清空选项
    clearable: boolean
    // 选项为空时显示的文字，也可以使用 empty 插槽设置
    noDataText: string
    // 是否可以筛选
    filterable: boolean
    // 搜索条件无匹配时显示的文字，也可以使用 empty 插槽设置
    noMatchText: string
    // 描述文字
    placeholder: string
    // 是否允许用户创建新条目， 当 filterable 必须为 true 时才会生效。
    allowCreate: boolean
    // 多选时用户最多可以选择的项目数， 为 0 则不限制
    multipleLimit: number
    // 多选时是否将选中值按文字的形式展示
    collapseTags: boolean
    // 下拉框是否与输入框同宽
    fitInputWidth: boolean
    // 选项数组
    options: Array<Option>
}

// 虚拟选择器
export interface VirtualizedSelectType extends BaseFormControlType {
    props: VirtualizedSelectPropsType
}

interface TimePickerPropsType extends BasePropsType {
    // 显示在输入框中的格式
    format: string
    // 是否为时间范围选择
    isRange: false
    // 文本框可输入
    editable: boolean
    // 完全只读
    readonly: boolean
    // 自定义清除当前选择的图标
    clearIcon: string
    // 自定义前缀图标
    prefixIcon: string
    // 是否可以清空选项
    clearable: boolean
    // 描述文字
    placeholder: string
}

// 时间选择器
export interface TimePickerType extends BaseFormControlType {
    props: TimePickerPropsType
}

interface TimePickerRangePropsType extends BasePropsType {
    // 显示在输入框中的格式
    format: string
    // 是否为时间范围选择
    isRange: true
    // 文本框可输入
    editable: boolean
    // 完全只读
    readonly: boolean
    // 自定义清除当前选择的图标
    clearIcon: string
    // 自定义前缀图标
    prefixIcon: string
    // 是否可以清空选项
    clearable: boolean
    // 是否使用箭头进行时间选择
    arrowControl: boolean
    // 选择范围时的分隔符
    rangeSeparator: string
    // 范围选择时结束日期的占位内容
    endPlaceholder: string
    // 范围选择时开始日期的占位内容
    startPlaceholder: string
}

// 时间选择器
export interface TimePickerRangeType extends BaseFormControlType {
    props: TimePickerRangePropsType
}

interface TimeSelectPropsType extends BasePropsType {
    // 结束时间
    end: string
    // 间隔时间
    step: string
    // 开始时间
    start: string
    // 最小时间，小于该时间的时间段将被禁用
    minTime: string
    // 最大时间，大于该时间的时间段将被禁用
    maxTime: string
    // 文本框可输入
    editable: boolean
    // 自定义清除当前选择的图标
    clearIcon: string
    // 自定义前缀图标
    prefixIcon: string
    // 是否可以清空选项
    clearable: boolean
    // 描述文字
    placeholder: string
}

// 时间下拉
export interface TimeSelectType extends BaseFormControlType {
    props: TimeSelectPropsType
}

interface DatetimePickerPropsType extends BasePropsType {
    // 完全只读
    type: string
    // 显示在输入框中的格式
    format: string
    // 文本框可输入
    editable: boolean
    // 自定义清除当前选择的图标
    clearIcon: string
    // 完全只读
    readonly: boolean
    // 自定义前缀图标
    prefixIcon: string
    // 是否可以清空选项
    clearable: boolean
    // 描述文字
    placeholder: string
    // 范围选择时结束日期的占位内容
    endPlaceholder: string
    // 选择范围时的分隔符
    rangeSeparator: string
    // 范围选择时开始日期的占位内容
    startPlaceholder: string
}

// 日期时间选择器
export interface DatetimePickerType extends BaseFormControlType {
    props: DatetimePickerPropsType
}

interface ButtonPropsType extends BasePropsType {
    // 图标
    icon: string
    // 类型
    type: string
    // 是否为朴素按钮
    plain: boolean
    // 是否为圆角按钮
    round: boolean
    // 是否为圆形按钮
    circle: boolean
    // 加载中
    loading: boolean
}

// 日期时间选择器
export interface ButtonType extends BaseFormControlType {
    // 按钮类型 submit-提交 reset-重置
    buttonType: string
    props: ButtonPropsType
}

interface ColType {
    props: {
        // 栅栏占据列数
        span: number
        // 栅格向左移动格数
        pull: number
        // 栅格向右移动格数
        push: number
        // 栅格左侧的间隔格数
        offset: number
    }
    // 该栅栏中的表单控件
    formItems: Array<
        | CheckboxType
        | InputType
        | PasswordType
        | TextareaType
        | InputNumberType
        | RadioType
        | RateType
        | SwitchType
        | SliderType
        | ColorType
        | SelectType
        | VirtualizedSelectType
        | TimePickerType
        | TimeSelectType
    >
}

interface ContainerPropsType extends BasePropsType {
    // 垂直排列方式
    align: string
    // 间隔
    gutter: number
    // 水平排列方式
    justify: string
}

// 日期时间选择器
export interface ContainerType extends BaseFormControlType {
    cols: Array<ColType>
    props: ContainerPropsType
}

/**
 * 表单项类型
 */
export interface FormItemTypes {
    baseItems: {
        // 输入框
        Input: InputType
        // 文本域
        Textarea: TextareaType
        // 密码框
        Password: PasswordType
        // 复选框
        Checkbox: CheckboxType
        // 步进器
        InputNumber: InputNumberType
        // 单选组
        Radio: RadioType
        // 评分
        Rate: RateType
        // 开关
        Switch: SwitchType
        // 滑块
        Slider: SliderType
    }
    selectItems: {
        // 下拉选择器
        Select: SelectType
        // 虚拟选择器
        VirtualizedSelect: VirtualizedSelectType
        // 颜色选择器
        Color: ColorType
        // 时间下拉
        TimeSelect: TimeSelectType
        // 时间选择器
        TimePicker: TimePickerType
        // 时间范围选择器
        TimePickerRange: TimePickerRangeType
        // 时间日期选择器
        DatetimePicker: DatetimePickerType
    }
    otherItems: {
        Button: ButtonType
        // 布局组件
        Container: ContainerType
    }
}

/**
 * 表单类型
 */
export interface FormConfigTypes {
    // 行内表单
    inline: boolean
    // 表单标签位置
    labelPosition: string
    // 表单域标签的后缀
    labelSuffix: string
    // 是否在输入框中显示校验结果反馈图标
    statusIcon: boolean
    // 是否显示必填字段的标签旁边的红色星号
    hideRequiredAsterisk: boolean
    // 是否显示校验错误信息
    showMessage: boolean
    // 尺寸
    size: string
    // 禁用
    disabled: boolean
}

export interface FormItemsType {
    currentItem: any
    changeItem: boolean
    items: Array<
        | CheckboxType
        | InputType
        | PasswordType
        | TextareaType
        | InputNumberType
        | RadioType
        | RateType
        | SwitchType
        | SliderType
        | ColorType
        | SelectType
        | VirtualizedSelectType
        | TimePickerType
        | TimeSelectType
    >
}
export interface FormDataType {
    data: any
}

export interface RootStateTypes {
    FormItem: FormItemTypes
    FormItems: FormItemsType
    FormData: FormDataType
    FormConfig: FormConfigTypes
}
