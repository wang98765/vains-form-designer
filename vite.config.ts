// import path from 'path'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import viteCompression from 'vite-plugin-compression'

// https://vitejs.dev/config/
export default defineConfig({
    base: '/vains-form-designer/',
    plugins: [
        vue() /*
        Components({
            resolvers: [ElementPlusResolver()],
        }),*/,
        viteCompression(),
    ],
    server: {
        // 取消访问IP限制
        host: '0.0.0.0',
    } /*,
    build: {
        lib: {
            entry: path.resolve(__dirname, 'src/export.ts'),
            name: 'vains-form-designer',
            fileName: (format) => `vains-form-designer.${format}.ts`,
        },
        rollupOptions: {
            // 确保外部化处理那些你不想打包进库的依赖
            external: ['vue', '@element-plus/icons', 'element-plus'],
            output: {
                // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
                globals: {
                    vue: 'Vue',
                },
            },
        },
    }*/,
})
