# vains-form-designer

#### 介绍
Vite + TypeScript + Vue3 + Element-plus制作的可视化表单生成器；

#### [预览地址](https://vains-sofia.gitee.io/vains-form-designer/)

#### 项目所需依赖
**注意：项目中需先添加ElementPlus与ElementIcon的依赖**

#### 快速开始

##### 下载

```shell
npm i vains-form-designer
```
##### main.ts/main.js引入

```javascript
import App from './App.vue';
import { createApp } from 'vue';
import ElementPlus from 'element-plus';
import * as ElIcons from "@element-plus/icons";
import VainsFormDesigner from 'vains-form-designer';

import 'element-plus/dist/index.css';
import 'vains-form-designer/dist/style.css';

const app = createApp(App);
// 加载图标
for (const icon in ElIcons) {
    // @ts-ignore
    app.component(icon, ElIcons[icon]);
}
app.use(ElementPlus);
app.use(VainsFormDesigner);
app.mount('#app');
```

##### 代码中使用

```vue
<script setup lang="ts">
import { ref } from 'vue';
const formData = ref({});
</script>

<template>
  <DesignerContainer :formData="formData" height="95vh" />
</template>
```

##### 根据已有配置生成

 ```vue
<script setup lang="ts">
import { ref } from 'vue'
const formData = ref({ Input_1637029608507: '1' })
const formItems = ref([
    {
        tag: 'el-input',
        type: 'Input',
        props: {
            size: 'medium',
            disabled: false,
            readonly: false,
            clearable: false,
            suffixIcon: '',
            prefixIcon: 'Avatar',
            placeholder: '输入框',
        },
        label: '输入框',
        modelValue: 'Input_1637029608507',
        selectCurrent: false,
    },
    {
        labelWidth: 100,
        tag: 'el-button',
        type: 'Button',
        props: {
            icon: '',
            type: '',
            size: 'medium',
            plain: false,
            round: false,
            circle: false,
            loading: false,
            disabled: false,
        },
        label: '按钮',
        buttonType: 'submit',
        modelValue: '',
        selectCurrent: true,
    },
])
const formSettings = ref({
    inline: false,
    labelPosition: 'right',
    labelWidth: 100,
    labelSuffix: '',
    statusIcon: false,
    hideRequiredAsterisk: false,
    showMessage: true,
    size: 'medium',
    disabled: false,
})
// 提交事件需添加提交类型的按钮方可触发(触发方式：@click)
const submit = () => {
    console.log(123)
}
</script>
<template>
    <Designer ref="designer" :formItems="formItems" :formData="formData" :formConfig="formSettings" @submit="submit" />
</template>

 ```

#### 属性介绍

##### 编辑器DesignerContainer

| 属性名    | 是否必填 | 说明                   | 字段类型 | 默认值                                           |
| --------- | -------- | ---------------------- | -------- | ------------------------------------------------ |
| height    | 否       | 编辑器高度             | string   | 96.8vh(单位请使用vh或px，百分比的情况下会有问题) |
| formData  | 否       | 表单双向绑定的json对象 | object   | {}                                               |
| formItems | 否       | 拖拽生成的表单组件列表 | Array    | []                                               |

##### 展示使用Designer

| 属性名     | 是否必填 | 说明                                             | 字段类型 | 默认值                                           |
| ---------- | -------- | ------------------------------------------------ | -------- | ------------------------------------------------ |
| height     | 否       | 编辑器高度                                       | string   | 96.8vh(单位请使用vh或px，百分比的情况下会有问题) |
| formData   | 是       | 表单双向绑定的json对象，复选和步进器要求有初始值 | object   | {}                                               |
| formConfig | 否       | 表单的配置，具体如下表所示                       | object   | 下表所示默认值                                   |
| formItems  | 是       | 表单显示的列表                                   | Array    | undefined                                        |

##### 表单配置项

| 属性名               | 说明                                                         | 字段类型 | 默认值   |
| -------------------- | ------------------------------------------------------------ | -------- | -------- |
| inline               | 行内表单模式                                                 | boolean  | false    |
| labelPosition        | 表单域标签的位置， 如果值为 left 或者 right 时，则需要设置 label-width | string   | 'right'  |
| labelSuffix          | 表单域标签的后缀                                             | string   | ''       |
| statusIcon           | 是否在输入框中显示校验结果反馈图标                           | boolean  | false    |
| hideRequiredAsterisk | 隐藏必填字段的标签旁边的红色星号                             | boolean  | false    |
| showMessage          | 是否显示校验错误信息                                         | boolean  | true     |
| size                 | 用于控制该表单内组件的尺寸                                   | string   | 'medium' |
| disabled             | 禁用                                                         | boolean  | false    |
##### 其它各组件的属性说明请参考ElementPlus文档

#### 事件 (DesignerContainer事件、Designer事件通用)
submit：提交事件，只有当添加了提交类型的按钮才会触发(触发方式：@click)

#### Designer组件属性
dynamicForm：表单实例，用于验证重置表单等，获取示例
```vue
<script setup lang="ts">
import { ref, onMounted, Ref } from 'vue'
const formData = ref({ Input_1637029608507: '1' })
const formItems = ref([
    {
        tag: 'el-input',
        type: 'Input',
        props: {
            size: 'medium',
            disabled: false,
            readonly: false,
            clearable: false,
            suffixIcon: '',
            prefixIcon: 'Avatar',
            placeholder: '输入框',
        },
        label: '输入框',
        modelValue: 'Input_1637029608507',
        selectCurrent: false,
    },
    {
        labelWidth: 100,
        tag: 'el-button',
        type: 'Button',
        props: {
            icon: '',
            type: '',
            size: 'medium',
            plain: false,
            round: false,
            circle: false,
            loading: false,
            disabled: false,
        },
        label: '按钮',
        buttonType: 'submit',
        modelValue: '',
        selectCurrent: true,
    },
])
const formSettings = ref({
    inline: false,
    labelPosition: 'right',
    labelWidth: 100,
    labelSuffix: '',
    statusIcon: false,
    hideRequiredAsterisk: false,
    showMessage: true,
    size: 'medium',
    disabled: false,
})
// 提交事件需添加提交类型的按钮方可触发(触发方式：@click)
const submit = () => {
    console.log(123)
}
const designer: Ref<HTMLElement | null> = ref<HTMLElement | null>(null)
onMounted(() => {
    // 获取并打印表单的实例
    // @ts-ignore
    console.log(designer.value.dynamicForm)
})
</script>
<template>
    <Designer ref="designer" :formItems="formItems" :formData="formData" :formConfig="formSettings" @submit="submit" />
</template>


 ```

#### DesignerContainer组件方法
getDynamicForm(): 获取表单实例对象
使用示例
 ```vue
 <template>
    <DesignerContainer ref="container" :formData="formData" :formItems="formItems" @submit="onSubmit" />
</template>

<script setup lang="ts">
import { ref, onMounted } from 'vue'

const container = ref(null)

onMounted(() => {
    // 调用方法获取表单实例对象
    // @ts-ignore
    console.log(container.value.getDynamicForm())
})

// 提交事件
const onSubmit = () => {
    console.log(formData.value)
}

// 表单数据
const formData = ref({
    Input_1637029608507: '',
})

// 表单列表
const formItems = ref([
    {
        labelWidth: 100,
        tag: 'el-input',
        type: 'Input',
        props: {
            size: 'medium',
            disabled: false,
            readonly: false,
            clearable: false,
            suffixIcon: '',
            prefixIcon: '',
            placeholder: '输入框',
        },
        label: '输入框',
        modelValue: 'Input_1637029608507',
        selectCurrent: false,
    },
])
</script>

<style>
html,
body {
    margin: 0;
    padding: 0;
}
</style>

 ```

#### 背景

前段时间在学习Vue3+TypeScript，想着边学边写，这样学起来会快一些，本项目完全就是学习中的一个产出，所以有些代码比较乱，特别是末期加的Vuex，导致项目更乱了...TypeScript也逐渐演化成AnyScript了( (╯°Д°)╯︵ ┻━┻ )<br>
本项目完全使用script setup的方式开发(这样开发起来很简单)


#### 目前支持的组件

复选框、输入框、步进器、密码框、单选框、评分、滑块、开关、文本域、颜色选择器、日期时间选择器、下拉选择器、虚拟选择器、时间选择器、时间范围选择、时间下拉选择、按钮、布局容器

#### 运行教程
先从[Gitee](https://gitee.com/vains-Sofia/vains-form-designer.git)中拉取代码

1.拉取代码之后运行命令安装依赖

```shell
npm install
```
2.依赖安装完成之后可运行命令启动项目

```shell
npm run dev
```
3.完成之后访问 http://localhost:3000/vains-form-designer/
查看效果

#### 发现问题
如果有什么问题或者好的建议请在评论区提出或提交issue<br>
最后，如果觉得项目还不错，请点个star ( φ(゜▽゜*)♪ )
